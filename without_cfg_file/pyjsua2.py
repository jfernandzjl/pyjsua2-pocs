import pjsua2 as pj
import sys
import random
from multiprocessing import Process, Event
import time


class Endpoint(pj.Endpoint):
    """
    This is high level Python object inherited from pj.Endpoint
    """
    instance = None

    def __init__(self):
        super().__init__()
        Endpoint.instance = self


# Call class
class Call(pj.Call):
    """
    High level Python Call object, derived from pjsua2's Call object.
    """
    def __init__(self, acc, peer_uri='', call_id=pj.PJSUA_INVALID_ID):
        super().__init__(acc, call_id)
        self.acc = acc
        self.peerUri = peer_uri
        self.connected = True
        self.onhold = False

    def onCallState(self, prm):
        ci = self.getInfo()
        self.connected = ci.state == pj.PJSIP_INV_STATE_CONFIRMED
        print(f"################ CONNECTED? => {self.connected} ################")
        if ci.state == pj.PJSIP_INV_STATE_DISCONNECTED:
            del self

    def onCallMediaState(self, prm):
        ci = self.getInfo()
        for mi in ci.media:
            if mi.type == pj.PJMEDIA_TYPE_AUDIO and \
                    (mi.status == pj.PJSUA_CALL_MEDIA_ACTIVE or
                     mi.status == pj.PJSUA_CALL_MEDIA_REMOTE_HOLD):
                m = self.getMedia(mi.index)
                am = pj.AudioMedia.typecastFromMedia(m)
                print(f"################ {am} ################")
                # connect ports
                Endpoint.instance.audDevManager().getCaptureDevMedia().startTransmit(am)
                am.startTransmit(Endpoint.instance.audDevManager().getPlaybackDevMedia())
                if mi.status == pj.PJSUA_CALL_MEDIA_REMOTE_HOLD and not self.onhold:
                    print(f"################ '{self.peerUri}' sets call onhold ################")
                    self.onhold = True
                elif mi.status == pj.PJSUA_CALL_MEDIA_ACTIVE and self.onhold:
                    print(f"################ '{self.peerUri}' sets call active ################")
                    self.onhold = False


class Account(pj.Account):
    """
    High level Python Account object, derived from pjsua2's Account object.
    """
    def __init__(self):  # , app):
        super().__init__()
        self.randId = random.randint(1, 9999)
        self.cfgChanged = False
        self.deleting = False
        # self.app = app
        self.calls = []

    def onRegState(self, prm):
        print(f"################ onRegState: {prm.reason} ################")

    def onIncomingCall(self, prm):
        print(f"################ ANSWERING INCOMING CALL: {prm.callId} ################")
        c = Call(acc=self, call_id=prm.callId)
        self.calls.append(c)
        call_prm = pj.CallOpParam()

        call_prm.statusCode = pj.PJSIP_SC_RINGING
        c.answer(call_prm)
        ring_time = 5
        print(f"################ RINGING FOR {ring_time}s ################")
        time.sleep(ring_time)

        call_prm.statusCode = pj.PJSIP_SC_OK
        c.answer(call_prm)
        print("################ CALL ANSWERED ################")

    def make_call(self):
        print("################ MAKING NEW CALL ################")
        c = Call(acc=self)
        self.calls.append(c)
        call_prm = pj.CallOpParam()
        peer_uri = 'sip:056003@pas-maintenance.com;transport=udp'
        c.makeCall(peer_uri, call_prm)


class Application(Process):
    """
    The Application main thread.
    """
    def __init__(self):
        super().__init__()

        # Instantiate endpoint
        self.ep = Endpoint()
        self.ep.libCreate()

        # Instantiate config objects
        self.epConfig = pj.EpConfig()
        self.accConfig = pj.AccountConfig()
        self.tpConfig = pj.TransportConfig()

        # Endpoint config parameters
        #   UserAgent
        self.epConfig.uaConfig.threadCnt = 0
        self.epConfig.uaConfig.mainThreadOnly = True
        self.epConfig.uaConfig.userAgent = "pyjsua-" + self.ep.libVersion().full
        #   Log
        self.epConfig.logConfig.filename = "pyjsua2.log"
        self.epConfig.logConfig.fileFlags = pj.PJ_O_APPEND
        self.epConfig.logConfig.level = 5
        self.epConfig.logConfig.consoleLevel = 3

        # Account config parameters
        self.accConfig.idUri = "sip:056004@pas-maintenance.com"
        self.accConfig.regConfig.registrarUri = "sip:pas-maintenance.com"
        creds = pj.AuthCredInfo("digest", "*", "056004", 0, "alfatec")
        self.accConfig.sipConfig.authCreds.append(creds)

        # Transport config parameters
        self.tpConfig.port = 5060

        # Handling quit
        self.quitting = Event()

        # Handling outgoing call
        self.make_call = Event()

        # Handling hangup
        self.make_hangup = Event()

        # Initialize account instance
        self.acc = None

    def run(self):

        # Initialize library
        self.ep.libInit(self.epConfig)

        # Create transport
        self.ep.transportCreate(pj.PJSIP_TRANSPORT_UDP, self.tpConfig)

        # Initialize and create account
        self.acc = Account()
        # self.accounts.append(acc)  # Do we need this to prevent the acc object to be destroyed by garbage collection?
        self.acc.create(self.accConfig)

        # Start library
        self.ep.libStart()

        # Start polling
        self._on_timer()

    def _on_timer(self):
        """Polling function"""
        try:
            while True:
                if self.make_call.is_set():
                    self.acc.make_call()
                    self.make_call.clear()
                    continue
                if self.acc.calls and self.make_hangup.is_set():
                    call_prm = pj.CallOpParam()
                    call_prm.statusCode = pj.PJSIP_SC_REQUEST_TERMINATED
                    for c in self.acc.calls:
                        try:
                            ci = c.getInfo()
                            print(f"--------------> Current call ID is ... {ci.id}")
                            c.hangup(call_prm)
                            if ci.state == pj.PJSIP_INV_STATE_DISCONNECTED:
                                del c
                        except pj.Error as err:
                            print(err)
                    self.make_hangup.clear()
                    continue
                elif self.quitting.is_set():
                    break
                self.ep.libHandleEvents(500)
            print("################ quitting EVENT HAS BEEN SET ################")
        except KeyboardInterrupt:
            print("################ KeyboardInterrupt HAS BEEN RECEIVED ################")

        self._on_close()

    def _on_close(self):
        print("################ DELETING ACCOUNTS ################")
        del self.acc
        self.ep.libDestroy()
        self.ep = None
        print("################ LIBRARY DESTROYED ################")


class AppControl:

    def __init__(self):
        self.app = Application()

    def start(self):

        self.app.start()

        while True:
            try:
                action = input("Take some action: ")
                if action.lower() == 'q':
                    self.app.quitting.set()
                    break
                elif action.lower() == 'm':
                    print("################ TRIGGERING OUTGOING CALL ################")
                    self.app.make_call.set()
                elif action.lower() == 'h':
                    print("################ HANGING UP CURRENT CALL ################")
                    self.app.make_hangup.set()
                else:
                    print('Unrecognized action. Try again.')
            except KeyboardInterrupt:
                self.app.quitting.set()
                break

        self.app.join()
        sys.exit(0)


if __name__ == '__main__':
    app_ctrl = AppControl()
    app_ctrl.start()
