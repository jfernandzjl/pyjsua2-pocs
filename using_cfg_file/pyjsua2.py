import pjsua2 as pj
import os
import sys
import random
import time
from multiprocessing import Process, Event


class Endpoint(pj.Endpoint):
    """
    This is high level Python object inherited from pj.Endpoint
    """
    instance = None

    def __init__(self):
        super().__init__()
        Endpoint.instance = self


def validateUri(uri):
    return Endpoint.instance.utilVerifyUri(uri) == pj.PJ_SUCCESS


def validateSipUri(uri):
    return Endpoint.instance.utilVerifySipUri(uri) == pj.PJ_SUCCESS


# Call class
class Call(pj.Call):
    """
    High level Python Call object, derived from pjsua2's Call object.
    """
    def __init__(self, acc, peer_uri='', call_id=pj.PJSUA_INVALID_ID):
        super().__init__(acc, call_id)
        self.acc = acc
        self.peerUri = peer_uri
        self.connected = True
        self.onhold = False

    def onCallState(self, prm):
        ci = self.getInfo()
        self.connected = ci.state == pj.PJSIP_INV_STATE_CONFIRMED
        print(f"################ CONNECTED? => {self.connected} ################")

    def onCallMediaState(self, prm):
        print(f"################ onCallMediaState: {prm} ################")
        ci = self.getInfo()
        for mi in ci.media:
            if mi.type == pj.PJMEDIA_TYPE_AUDIO and \
                    (mi.status == pj.PJSUA_CALL_MEDIA_ACTIVE or
                     mi.status == pj.PJSUA_CALL_MEDIA_REMOTE_HOLD):
                m = self.getMedia(mi.index)
                am = pj.AudioMedia.typecastFromMedia(m)

                # connect ports
                Endpoint.instance.audDevManager().getCaptureDevMedia().startTransmit(am)
                am.startTransmit(Endpoint.instance.audDevManager().getPlaybackDevMedia())

                if mi.status == pj.PJSUA_CALL_MEDIA_REMOTE_HOLD and not self.onhold:
                    print(f"################ '{self.peerUri}' sets call onhold ################")
                    self.onhold = True
                elif mi.status == pj.PJSUA_CALL_MEDIA_ACTIVE and self.onhold:
                    print(f"################ '{self.peerUri}' sets call active ################")
                    self.onhold = False


class Account(pj.Account):
    """
    High level Python Account object, derived from pjsua2's Account object.
    """
    def __init__(self, app):
        super().__init__()
        self.app = app
        self.randId = random.randint(1, 9999)
        # self.cfg = pj.AccountConfig()
        self.cfgChanged = False
        self.deleting = False

    def onRegState(self, prm):
        print(f"################ onRegState: {prm.reason} ################")

    def onIncomingCall(self, prm):
        print("################ ANSWERING INCOMING CALL ################")
        c = Call(acc=self, call_id=prm.callId)
        call_prm = pj.CallOpParam()

        call_prm.statusCode = 180
        c.answer(call_prm)
        ring_time = 2
        print(f"################ RINGING FOR {ring_time}s ################")
        time.sleep(ring_time)

        call_prm.statusCode = 200
        c.answer(call_prm)
        print("################ CALL ANSWERED ################")


# Transport setting
class SipTransportConfig:
    def __init__(self, typ, enabled):
        self.type = typ
        self.enabled = enabled
        self.config = pj.TransportConfig()

    def read_object(self, node):
        child_node = node.readContainer("SipTransport")
        self.type = child_node.readInt("type")
        self.enabled = child_node.readBool("enabled")
        self.config.readObject(child_node)


# Account setting
class AccConfig:
    def __init__(self):
        self.enabled = True
        self.config = pj.AccountConfig()

    def read_object(self, node):
        acc_node = node.readContainer("Account")
        self.enabled = acc_node.readBool("enabled")
        self.config.readObject(acc_node)


# Master settings
class AppConfig:
    def __init__(self):
        self.epConfig = pj.EpConfig()
        self.udp = SipTransportConfig(pj.PJSIP_TRANSPORT_UDP, True)
        self.accounts = []

    def load_file(self, file):
        json = pj.JsonDocument()
        json.loadFile(file)
        root = json.getRootContainer()
        self.epConfig = pj.EpConfig()
        self.epConfig.readObject(root)

        tp_node = root.readArray("transports")
        self.udp.read_object(tp_node)

        acc_node = root.readArray("accounts")
        while acc_node.hasUnread():
            acfg = AccConfig()
            acfg.read_object(acc_node)
            self.accounts.append(acfg)


class Application(Process):
    """
    The Application main thread.
    """
    def __init__(self):
        super().__init__()

        # Accounts
        self.accList = []

        # Instantiate endpoint
        self.ep = Endpoint()
        self.ep.libCreate()

        # Handling quit
        self.quitting = Event()

        # Default config
        self.appConfig = AppConfig()

    def run(self, cfg_file='pyjsua2.json'):

        # Load config
        if cfg_file and os.path.exists(cfg_file):
            self.appConfig.load_file(cfg_file)

        # Initialize library
        self.appConfig.epConfig.uaConfig.userAgent = "pyjsua-" + self.ep.libVersion().full
        self.ep.libInit(self.appConfig.epConfig)

        # Create UDP transport
        if self.appConfig.udp.enabled:
            self.ep.transportCreate(self.appConfig.udp.type, self.appConfig.udp.config)

        # Add accounts
        for acfg in self.appConfig.accounts:
            if acfg.enabled:
                self._create_acc(acfg.config)

        # Start library
        self.ep.libStart()

        # Start polling
        self._on_timer()

    def _create_acc(self, acc_cfg):
        acc = Account(self)
        acc.cfg = acc_cfg
        self.accList.append(acc)
        acc.create(acc.cfg)
        acc.cfgChanged = False

    def _on_timer(self):
        try:
            while not self.quitting.is_set():
                self.ep.libHandleEvents(10000)
            print("################ QUITTING EVENT HAS BEEN SET ################")
        except KeyboardInterrupt:
            print("################ KEYBOARDINTERRUPT HAS BEEN RECEIVED ################")
        finally:
            self._on_close()

    def _on_close(self):
        self.ep.libDestroy()
        self.ep = None
        print("################ LIBRARY DESTROYED!! ################")


class AppControl:

    def __init__(self):
        self.app = Application()

    def start(self):

        self.app.start()

        while True:
            print("Take some action: ")
            try:
                var = input()
                if var == 'q':
                    self.app.quitting.set()
                    break
                print('Unrecognized action. Try again.')
            except KeyboardInterrupt:
                self.app.quitting.set()
                break

        self.app.join()
        sys.exit(0)


if __name__ == '__main__':
    app_ctrl = AppControl()
    app_ctrl.start()
